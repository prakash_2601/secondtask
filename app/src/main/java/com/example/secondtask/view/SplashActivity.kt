package com.example.secondtask.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.secondtask.BR
import com.example.secondtask.R
import com.example.secondtask.baseActivity.BaseActivity
import com.example.secondtask.baseActivity.BaseNavigator
import com.example.secondtask.databinding.ActivitySplashBinding
import com.example.secondtask.view.home.HomeActivity

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(),BaseNavigator {
    private lateinit var viewModel: SplashViewModel

    override fun getBindingVariable(): Int {
        return BR.splashVM
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun getViewModel(): SplashViewModel {
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        return viewModel
    }

    override fun onClickView(v: View?) {

    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setNavigator(this)
        val splashTimeOut : Long=5000 // 5 sec

        // 5 seconds wait for same activity then it will automatically go for homePage

        Handler().postDelayed({
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }, splashTimeOut)
    }

}