package com.example.secondtask.view

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.secondtask.baseActivity.BaseNavigator
import com.example.secondtask.baseActivity.BaseViewModel
import com.example.secondtask.model.HomeModuleResponse
import com.example.secondtask.repo.HomeControllerRepository
import com.example.secondtask.utilities.Resource
import com.example.secondtask.utilities.SingleLiveData

class SplashViewModel(application: Application) : BaseViewModel<BaseNavigator>(application) {

    val progressBarVisible = ObservableField<Boolean>(false)
    val getImageListDataResponse = MutableLiveData<Resource<List<HomeModuleResponse>>>()
    val pageNumber = ObservableField<Int>(1)
    val limit = ObservableField<Int>(20)
    val mShowProgressBar = SingleLiveData<Boolean>()
    val swipeRefreshBoolean = ObservableField<Boolean>(false)

    fun getImageListApi(){
        if (pageNumber.get() == 1 && !swipeRefreshBoolean.get()!!) {mShowProgressBar.value = true}
        HomeControllerRepository.getInstance().getImageListApi(getImageListDataResponse,pageNumber.get()!!,limit.get()!!)
    }

}