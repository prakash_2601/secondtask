package com.example.secondtask.view.home

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.secondtask.BR
import com.example.secondtask.R
import com.example.secondtask.adapter.BaseRecyclerViewAdapter
import com.example.secondtask.adapter.OnDataBindCallback
import com.example.secondtask.baseActivity.BaseActivity
import com.example.secondtask.baseActivity.BaseNavigator
import com.example.secondtask.databinding.ActivityHomeBinding
import com.example.secondtask.databinding.HomeAdapterlistBinding
import com.example.secondtask.model.HomeModuleResponse
import com.example.secondtask.view.SplashViewModel
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<ActivityHomeBinding, SplashViewModel>(), BaseNavigator {

    private lateinit var viewModel: SplashViewModel
    private var isLoader: Boolean = false

    private var adapterHome : BaseRecyclerViewAdapter<HomeModuleResponse, HomeAdapterlistBinding>? =
        null

    override fun getBindingVariable(): Int {
        return BR.homeVM
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun getViewModel(): SplashViewModel {
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        return viewModel
    }

    override fun onClickView(v: View?) {

    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setNavigator(this)
        viewModel.getImageListApi()

        viewModel.mShowProgressBar.observeEvent(this, Observer {
            it.let {
                showProgressBar(progressBarLayout)
            }
        })

        mViewDataBinding?.rvHome?.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,false)

        adapterHome = BaseRecyclerViewAdapter(R.layout.home_adapterlist, BR.homeAdapterVM, ArrayList(),
            null, object : OnDataBindCallback<HomeAdapterlistBinding> {
                override fun onItemClick(view: View, position: Int, v: HomeAdapterlistBinding) {
                    when(view.id){
                        R.id.card_view -> {
                            // while click the particular cell show the dialog
                            dialogPopup(v.tvDescription.text.toString().substring(13,v.tvDescription.text.length))
                        }
                    }
                }

                override fun onDataBind(v: HomeAdapterlistBinding, onClickListener: View.OnClickListener) {
                    v.cardView.setOnClickListener(onClickListener)
                }
            })

        mViewDataBinding?.rvHome?.adapter = adapterHome

        // recycler view scrollDown PageNation functionality handling
        mViewDataBinding?.rvHome?.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0) {
                    val visibleItemCount = recyclerView.childCount
                    val totalItemCount = recyclerView.adapter?.itemCount
                    val firstVisibleItemPosition =
                        (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if ((isLoader) && ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount!!)) {
                        isLoader = false
                        viewModel.pageNumber.set(viewModel.pageNumber.get()!!.plus(1))
                        viewModel.progressBarVisible.set(true)

                        viewModel.getImageListApi()
                    } else if (!(isLoader) && !((visibleItemCount + firstVisibleItemPosition) >= totalItemCount!!)) {
                        isLoader = true
                    }
                }
            }
        })
         // pullDown refresh the page
        mViewDataBinding?.swipeRefreshLayout?.setOnRefreshListener {
            viewModel.swipeRefreshBoolean.set(true)
            viewModel.pageNumber.set(1)
            viewModel.getImageListApi()
        }

        mViewDataBinding?.swipeRefreshLayout?.setColorSchemeColors(
            resources.getColor(R.color.colorPrimary)
        )
         // adapter add and clearData Functionality
        viewModel.getImageListDataResponse.observe(this, Observer {
            it.let {
                dismissProgressBar()

                viewModel.progressBarVisible.set(false)
                viewModel.swipeRefreshBoolean.set(false)

                if (mViewDataBinding?.swipeRefreshLayout?.isRefreshing == true) {
                    mViewDataBinding?.swipeRefreshLayout?.isRefreshing = false
                    adapterHome?.clearDataSet()
                }
                
                if (!it.data.isNullOrEmpty()) {
                    adapterHome?.addDataSet(it.data)
                }else{
                    if (!it.Message.isNullOrEmpty()) {putToast(it.Message)}
                    viewModel.pageNumber.set(viewModel.pageNumber.get()?.minus(1))
                }
            }
        })

    }

    fun dialogPopup(message : String) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setMessage(message)
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, resources.getString(R.string.ok)) { dialog, which ->
            alertDialog.dismiss()
        }
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }
}