package com.clearthinking.data.remote.client

interface InternetConnectionListener {
    fun onInternetConnection(isInternetAvailable: Boolean)
}