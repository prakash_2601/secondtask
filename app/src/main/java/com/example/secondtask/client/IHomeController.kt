package com.example.secondtask.client

import com.example.secondtask.model.HomeModuleResponse
import com.example.secondtask.repo.BaseResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface IHomeController {

    @GET("v2/list?")
    fun getImageList(@Query("page") page : Int, @Query("limit") limit : Int) : Call<List<HomeModuleResponse>>
}