package com.example.secondtask.client

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class ApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val originalRequest: Request = chain!!.request()
        val newRequest = originalRequest.newBuilder()
            .build()

        return chain.proceed(newRequest)

    }

}