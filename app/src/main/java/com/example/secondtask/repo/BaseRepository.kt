package com.example.secondtask.repo

import androidx.lifecycle.MutableLiveData
import com.example.secondtask.R
import com.example.secondtask.SecondTaskApplication
import com.example.secondtask.utilities.Resource
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException

open class BaseRepository {

    private val noNetworkMsg = "No internet connection or network failure"

    fun <T> getErrorResponse(responseBody: ResponseBody): BaseResponse<T> {
        val truckErrorResponse: BaseResponse<T>
        val errorMsg = SecondTaskApplication.getInstance()?.resources?.getString(R.string.response_error)!!

        truckErrorResponse = try {
            val jObjError = JSONObject(responseBody.string())
            val message = if (jObjError.has("message")) jObjError.getString("message") else errorMsg
            val isSuccess = if (jObjError.has("isSuccess")) jObjError.getBoolean("isSuccess") else false
            BaseResponse(
                Message = message!!,
                IsSuccess = isSuccess,
                data = null
            )
        } catch (e: JSONException) {
            BaseResponse(
                IsSuccess = false,
                Message = errorMsg,
                data = null)
        }
        return truckErrorResponse
    }

    fun <T> getCallbackList(responseData: MutableLiveData<Resource<List<T>>>): Callback<List<T>> {
        return object : Callback<List<T>> {
            override fun onFailure(call: Call<List<T>>, t: Throwable) {
                val msg = if (t is IOException) noNetworkMsg else t.message!!
                responseData.value = Resource.failure(msg, null)
            }

            override fun onResponse(call: Call<List<T>>, response: Response<List<T>>) {

                if (response.isSuccessful && response.body() != null) {
                    responseData.value = Resource.success(response.body()!!)
                } else if (response.errorBody() != null) {
                    val errorResponse = getErrorResponse<List<T>>(response.errorBody()!!)
                    //responseData.value = Resource.error(errorResponse.Message, errorResponse)
                }
            }
        }
    }


    fun prepareTextRequestBody(data: String?): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), data ?: "")
    }

    fun prepareFilePart(partName: String, file: File?): MultipartBody.Part? {
        val requestBody = getRequestBody(file)
        return if (requestBody != null) {
            MultipartBody.Part.createFormData(partName, file!!.name, requestBody)
        } else {
            null
        }
    }

    private fun getRequestBody(file: File?): RequestBody? {
        if (file != null) {
            return if (file.exists()) {
                RequestBody.create(MediaType.parse("image/*"), file)
            } else {
                null
            }
        } else {
            return null
        }

    }

}