package com.example.secondtask.repo

import androidx.lifecycle.MutableLiveData
import com.example.secondtask.client.SecondTaskClientBuilder
import com.example.secondtask.model.HomeModuleResponse
import com.example.secondtask.utilities.Resource

class HomeControllerRepository : BaseRepository() {

    private var apiService = SecondTaskClientBuilder.Companion.ServicesApiInterface

    companion object {
        @Volatile
        private var instance: HomeControllerRepository? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: HomeControllerRepository().also { instance = it }
        }
    }

    fun getImageListApi(responseData : MutableLiveData<Resource<List<HomeModuleResponse>>>, page : Int, limit : Int){
        apiService.homeUser()?.getImageList(page = page,limit = limit)?.enqueue(getCallbackList(responseData))
    }
}