package com.example.secondtask.repo

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(@SerializedName("isSuccess")
                           var IsSuccess : Boolean,
                           @SerializedName("message")
                           var Message : String,
                           @SerializedName("data")
                           var data: T?
)