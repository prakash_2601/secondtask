package com.example.secondtask

import android.app.Application
import androidx.databinding.DataBindingUtil
import com.example.secondtask.component.BaseDataBindingComponent

class SecondTaskApplication  : Application() {
    companion object{
        private var mInstance : SecondTaskApplication? = null

        fun getInstance() : SecondTaskApplication? {
            return mInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        DataBindingUtil.setDefaultComponent(BaseDataBindingComponent())
    }
}