package com.example.secondtask.component

import androidx.databinding.DataBindingComponent
import com.example.secondtask.component.implemantation.IImageViewBinding
import com.example.secondtask.component.modules.BaseImageViewBinding


class BaseDataBindingComponent : DataBindingComponent {

    override fun getIImageViewBinding(): IImageViewBinding {
        return BaseImageViewBinding()
    }


}
