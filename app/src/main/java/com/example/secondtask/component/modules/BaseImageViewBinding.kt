package com.example.secondtask.component.modules

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.secondtask.R
import com.example.secondtask.component.implemantation.IImageViewBinding

class BaseImageViewBinding : IImageViewBinding {

    override fun setImageFromUrl(imageView: ImageView, filePath: String?) {
        val option: RequestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .placeholder(R.drawable.kotlinimg)
                .fitCenter()

        Glide.with(imageView.context)
                .load(filePath)
                .apply(option)
                .into(imageView)
    }
}
