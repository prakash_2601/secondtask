package com.example.secondtask.model

import com.google.gson.annotations.SerializedName

data class HomeModuleResponse(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("author")
    val author: String? = null,
    @SerializedName("width")
    val width: Int? = null,
    @SerializedName("height")
    val height: Int? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("download_url")
    val downloadUrl: String? = null
)